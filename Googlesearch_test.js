Feature('Googlesearch');

Scenario('TC1-Google search', ({ I }) => {
    I.amOnPage('/');
    I.amOnPage('https://opensource-demo.orangehrmlive.com');
    // I.seeElement("//input[@name='q']");
    // I.fillField("//input[@name='q']","Googol");
    // I.click("btnK");
    //https://opensource-demo.orangehrmlive.com/

    I.wait(3);
});


Scenario('Testcase2-Test', ({ I })=>{
    I.amOnPage('https://opensource-demo.orangehrmlive.com');
    
    I.see('LOGIN Panel');   //Assertion
    I.fillField("//input[@id='txtUsername']", "Admin"); //XPATH
    I.fillField("#txtPassword", "admin123");  //CSS
    I.click("#btnLogin");   //ID
    I.see("Dashboard"); //Assertion
    I.dontSee("LOGIN Panel");   //Assertion

})
